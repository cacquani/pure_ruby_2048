require 'minitest/autorun'
require 'minitest/pride'

require '2048.rb'

class Test2048 < Minitest::Test
  def test_initialize_should_create_grid_and_painter
    game = Game::Game2048.new
    assert_equal Game::Painter, game.painter.class
    assert_equal Game::Grid, game.grid.class
  end
end
