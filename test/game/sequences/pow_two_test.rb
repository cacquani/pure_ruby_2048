require 'minitest/autorun'
require 'minitest/pride'

require 'game/sequences/pow_two.rb'

class TestPowTwo < Minitest::Test
  def setup
  end

  def test_should_initialize_at_2_when_weight_less_than_12
    0.upto(11) do |weight|
      values = []
      0.upto(50) do |i|
        values << Game::PowTwo.new.starter(weight).value
      end
      assert values.include?(2)
      assert_equal false, values.include?(4)
    end
  end

  def test_should_initialize_2s_and_4s_when_weight_12_or_more
    12.upto(16) do |weight|
      values = []
      0.upto(50) do |i|
        values << Game::PowTwo.new.starter(weight).value
      end
      assert values.include?(2)
      assert values.include?(4)
    end
  end
end
