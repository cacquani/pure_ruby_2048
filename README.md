# 2048 Ruby Clone #

Developed as a nice way to get rid of an obsession for that silly game (silly as a concept, anyway).

To play, enter the game directory and run: 

> rake game

## DONE at 2014-10-03 ##

* make sure it doesnt re-merge a tile which is the result of another merge in this very round
* make sure it doesn't inject if there is no move done
* add current score (when you merge two tiles, it adds the sum of the two tile values to the score)
* add failure condition
* devolve sequence to dynamic sequence module
* add victory condition
* save score to file
* add high score
* refine failure condition. make it fail only if there are no moves left 
* weight the randomizer in the pow2 sequencer so that you get new '2' tiles more often than '4' tiles. the '4' tiles are put in game when the player has more than 12 tiles in the checker
* add default colors to painter

## TODO: ##

* clean the code
* experiment with single responsibility
* add tests for the working feature - so that I can add other things without having to worry about breaking the old stuff
* keep adding tests when I develop the new stuff
* once you have won, it should not tell you that you lose when you have no more moves.
* add a 'continue/new game' option when you win. this will need to update the high score
* add a 'new game' option when you lose. this will need to update the high score
* ask for 'player name' at the beginning of the game and put it in the scores file and in the 'high score' notice
* move color definitions to sequence
* add a 'hide/show' functionality that shows some sort of progress indicator when hidden
* add a new game/resume/save menu
* add a Fibonacci version
