require 'io/console'
require './lib/game/grid.rb'
require './lib/game/painter.rb'

module Game
  class Game2048
    # I/O class for the game
    # This class's only responsibility is to wrap the game object,
    # pass over the keyboard input and exit the program 

    attr_reader :grid, :painter

    def initialize
      @grid = Game::Grid.new
      @painter = Game::Painter.new(self.grid)
      self.paint
    end

    def run
      loop { break if ( step ) }
      self.paint_result
      grid.teardown
    end

    def step
      key = STDIN.getch.ord
      grid.move key
      self.paint
      return [113, 101].include?(key) || !grid.result
    end

    # INTERFACE
    def paint
      self.painter.paint
    end

    def paint_result
      self.painter.results
    end
  end
end
