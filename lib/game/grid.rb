require 'csv'
require './lib/game/grid/border_tile.rb'
require './lib/game/grid/tile.rb'
require './lib/game/sequences/pow_two_colour.rb'

module Game
  class Grid
    # Main grid where the game runs

    BASENAME = "./results/game.csv"
    UP    = 65
    DOWN  = 66
    RIGHT = 67
    LEFT  = 68
    MAX_TILES = 16
    DIRECTIONS = {
      UP    => [ -1,  0 ],
      DOWN  => [  1,  0 ],
      RIGHT => [  0,  1 ],
      LEFT  => [  0, -1 ]
    }
    BOUNDARIES = {
      UP    => { fo: 1, to: 3, fi: 0, ti: 3 },
      DOWN  => { fo: 2, to: 0, fi: 0, ti: 3 },
      RIGHT => { fo: 2, to: 0, fi: 0, ti: 3 },
      LEFT  => { fo: 1, to: 3, fi: 0, ti: 3 }
    }

    attr_accessor :grid, :tiles, :score, :won
    attr_reader   :sequence, :maxscore, :result

    def initialize()
      initialize_grid
      self.tiles = 0
      self.score = 0
      self.won = false
      @maxscore = 0
      if FileTest.exist?(Grid::BASENAME)
        CSV.foreach(Grid::BASENAME) do |line|
          score = line[1].to_i
          @maxscore = score if score > @maxscore
        end
      end
      @sequence = Game::PowTwoColour.new
      Tile.new(self)
      Tile.new(self)
    end

    def teardown
      head = (FileTest.exist?(Grid::BASENAME) ? false : ["Date", "Score"])
      CSV.open(Grid::BASENAME, "a+") do |csv|
        csv << head if head
        csv << [Time.now, self.score]
      end
    end

    def[](y,x = nil)
      row = grid[y]
      return x ? row[x] : row
    end

    def move(direction)
      prime
      return @result = true unless valid?(direction)
      do_move(direction)
      return @result = cleanup || true
    end

    def full?
      return self.tiles == 16
    end

    def can_move?
      return true unless self.full?
      scan_grid(fo: 0, to: 3, fi: 0, ti: 3) do |y,x|
        return true if grid[y][x].can_move?
      end
      return false
    end

    def moved
      @has_moved = true
    end

    def unmoved
      @has_moved = false
    end

    def moved?
      @has_moved
    end

    private
    def initialize_grid
      @borders = {
        UP    => { 
                    0 => BorderTile.new(self, :top, nil, 0), 
                    1 => BorderTile.new(self, :top, nil, 1), 
                    2 => BorderTile.new(self, :top, nil, 2), 
                    3 => BorderTile.new(self, :top, nil, 3) 
        },
        DOWN  => { 
                    0 => BorderTile.new(self, :bottom, nil, 0), 
                    1 => BorderTile.new(self, :bottom, nil, 1), 
                    2 => BorderTile.new(self, :bottom, nil, 2), 
                    3 => BorderTile.new(self, :bottom, nil, 3) 
        },
        RIGHT => { 
                    0 => BorderTile.new(self, :right, 0, nil), 
                    1 => BorderTile.new(self, :right, 1, nil), 
                    2 => BorderTile.new(self, :right, 2, nil), 
                    3 => BorderTile.new(self, :right, 3, nil) 
        },
        LEFT  => { 
                    0 => BorderTile.new(self, :left, 0, nil), 
                    1 => BorderTile.new(self, :left, 1, nil), 
                    2 => BorderTile.new(self, :left, 2, nil), 
                    3 => BorderTile.new(self, :left, 3, nil) 
        }
      }

      self.grid = Array.new(4){ Array.new(4, nil)}

    end

    def valid?(direction)
      return [Grid::UP, Grid::DOWN, Grid::LEFT, Grid::RIGHT].include?(direction)
    end

    def vertical?(direction)
      [Grid::UP, Grid::DOWN].include?(direction)
    end

    def scan(f: 0, t: 3)
      m = (t < f ? :downto : :upto)
      f.send(m, t) do |i|
        yield(i)
      end
    end

    def scan_grid(fo: 0, to: 3, fi: 0, ti: 3)
      scan(f: fo, t: to) do |outer|
        scan(f: fi, t: ti) do |inner|
          yield(outer, inner)
        end
      end
    end

    def prime
      scan_grid(fo: 0, to: 3, fi: 0, ti: 3) do |y,x|
        cell = grid[y][x]
        cell.clean! if cell
      end
    end

    def do_move(direction)
      boundaries = Grid::BOUNDARIES[direction]
      scan_grid(boundaries) do |outer, inner|
        vertical?(direction) ?
            do_slide(x: inner, y: outer, d: direction) :
            do_slide(x: outer, y: inner, d: direction)
      end 
    end

    def do_slide(x: nil, y: nil, d: nil)
      cell = grid[y][x]
      cell.slide(d) if cell
    end

    def cleanup
      if self.moved?
        self.unmoved
        Tile.new(self)
        @result = (self.full? ? self.can_move? : true)
      end
    end

  end
end
