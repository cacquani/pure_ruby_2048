require './lib/game/grid.rb'

module Game
  class Grid
    class BorderTile
      BORDERS = [:top, :right, :bottom, :left]

      attr_reader :grid, :movable, :border, :up, :down, :left, :right
      attr_accessor :x, :y

      def initialize(grid, border, y, x)
        border = border.to_sym
        @grid = grid
        @border = BORDERS.include?(border) ? border : nil
        @movable = false
        self.y = y
        self.x = x
      end

      def chain
      end
    end
  end
end
