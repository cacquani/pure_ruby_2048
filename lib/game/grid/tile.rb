require './lib/game/grid/border_tile.rb'
require './lib/game/grid.rb'

module Game
  class Grid
    class Tile < Grid::BorderTile
      attr_accessor :val, :gid, :tainted
    
      def to_s; val.to_s; end
      def colour; val.colour; end
    
      def initialize(grid)
        @grid = grid
        @movable = true
        return false if self.grid.full?
        y, x = rand(4), rand(4)
        while(self.grid[y, x] != nil)
          y, x = rand(4), rand(4)
        end
        self.y = y
        self.x = x
        self.val = self.grid.sequence.starter(self.grid.tiles)
        self.gid = self.grid.tiles
        self.grid[y][x] = self
        self.grid.tiles = self.grid.tiles + 1
      end
    
      def destroy!
        self.grid[self.y][self.x] = nil
        self.grid.tiles = self.grid.tiles - 1
      end
    
      def taint!; self.tainted = true; end
      def clean!; self.tainted = false; end
      def tainted?; self.tainted == true; end
    
      def slide(direction)
        cury,  curx = self.y, self.x
        plusy, plusx = Grid::DIRECTIONS[direction]
        while((cury+plusy).between?(0, 3) && (curx+plusx).between?(0, 3))
          cury, curx = cury + plusy, curx + plusx
          break if self.grid[cury][curx]
        end
        if(self.grid[cury][curx])
          if (self.grid[cury][curx].val.mergeable(self.val) &&
              !self.grid[cury][curx].tainted?)
            self.grid[cury][curx].val = self.val + self.grid[cury][curx].val
            self.grid.score += self.grid[cury][curx].val.value
            self.grid[cury][curx].taint!
            self.destroy!
            self.grid.moved
          elsif(cury - plusy != self.y || curx - plusx != self.x)
            self.grid[self.y][self.x] = nil
            self.y, self.x = cury - plusy, curx - plusx
            self.grid[self.y][self.x] = self
            self.grid.moved
          end
        elsif(cury != self.y || curx != self.x)
          self.grid[self.y][self.x] = nil
          self.y, self.x = cury, curx
          self.grid[self.y][self.x] = self
          self.grid.moved
        end
      end
    
      def can_move?
        return false unless self.movable
        can_move = false
        can_move = can_move || (self.y - 1 >= 0 && self.grid[self.y - 1][self.x].val.mergeable(self.val))
        can_move = can_move || (self.y + 1 <= 3 && self.grid[self.y + 1][self.x].val.mergeable(self.val))
        can_move = can_move || (self.x - 1 >= 0 && self.grid[self.y][self.x - 1].val.mergeable(self.val))
        can_move = can_move || (self.x + 1 <= 3 && self.grid[self.y][self.x + 1].val.mergeable(self.val))
        return can_move
      end
    end
  end
end
