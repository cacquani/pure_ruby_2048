require './lib/game/sequence.rb'

module Game
  class PowTwo < Sequence
    VICTORY_CONDITION = 2048

    def initialize
      super
    end

    def starter(weight = 0)
      PowTwo::Value.new(self, weight)
    end

    class Value < Sequence::Value
      def initialize(sequence, weight = 0)
        self.sequence = sequence
        self.value = (weight < 12 ? 2 : (rand(2) + 1) * 2)
      end

      def mergeable(compare)
        return false unless compare.is_a? Value
        return self.value == compare.value
      end

      def +(addend)
        return self unless addend.is_a? Value
        self.value += addend.value
        self.sequence.win if value == VICTORY_CONDITION
        return self
      end
    end
  end
end
