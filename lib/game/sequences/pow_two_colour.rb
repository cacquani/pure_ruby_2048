require './lib/game/sequences/pow_two.rb'

module Game
  class PowTwoColour < PowTwo
    VICTORY_CONDITION = 2048
    COLOURS = {
      2 => "\e[31m",
      4 => "\e[33m",
      8 => "\e[32m",
      16 => "\e[36m",
      32 => "\e[34m",
      64 => "\e[35m",
      128 => "\e[1;35m",
      256 => "\e[1;34m",
      512 => "\e[1;36m",
      1024 => "\e[1;32m",
      2048 => "\e[1;33m",
      4096 => "\e[1;31m",
    }

    def starter(weight = 0)
      PowTwoColour::Value.new(self, weight)
    end

    class Value < PowTwo::Value
      def colour; COLOURS[self.value]; end
    end
  end
end
