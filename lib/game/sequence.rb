module Game
  class Sequence
    VICTORY_CONDITION = 0
    BASECOLOR = "\e[0m"
    attr_reader :sequence, :won
    def initialize
      @sequence = {}
      @won = false
    end

    def starter
      ::Value.new
    end

    def win
      @won = true
    end

    class Value
      attr_accessor :value, :sequence
      def to_s; self.value.to_s; end
      def colour; BASECOLOUR; end

      def initialize(sequence)
        self.sequence = sequence
        self.value = 0
      end

      def mergeable(compare)
        self.value == compare
      end

      def +(addend)
        self.value += addend
      end
    end
  end
end
