module Game
  class Painter
    attr_reader :grid

    def initialize(grid)
      @grid = grid
    end

    def paint
      clear
      puts "|" + "-----|" * 4
      0.upto(3) { |row| draw_row(row) }
      draw_scores
    end

    def results
      puts "Game ended with #{grid.score} points"
      puts grid.result ? "You quit" : (grid.sequence.won ? "You won" : "You lose")
      puts grid.tiles
    end

    private
    def clear; puts "\e[H\e[2J"; end

    def draw_scores
      puts
      puts "high score #{grid.maxscore}" if grid.respond_to?(:maxscore)
      puts "current score #{grid.score}" if grid.respond_to?(:score)
      puts "You won!" if grid.sequence.won
    end

    def draw_row(row)
      puts "|     |     |     |     |"
      draw_row_content(row)
      puts "|-----|-----|-----|-----|"
    end

    def draw_row_content(row)
      puts "|" + (0.upto(3).collect do |col|
        cell = grid[row,col]
        "#{cell ? cell.colour : "\e[1m"}#{cell.to_s.rjust(5)}\e[0m|"
      end).join('')
    end
  end
end
